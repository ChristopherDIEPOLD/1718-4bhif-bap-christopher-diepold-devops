This project is the first iteration of the DevOps lifecycle we learned in BAP.

#What it contains:

Jenkins Blue and Green deployment with yaml and Dockerfiles (Requires 2 seperate folders with the Dockerfiles in them)

A Gitlab yaml file

A Nexus yaml file


#How to run:
Clone the repo and run the setup.sh script.

The blue and green folders with the Dockerfiles are cloned with the repo
All other needed folders are created by the scirpt.

#Mapped Ports
Gitlab Port: 180
Jenkins blue (primary Jenkins): 18080
Jenkins green (secondary Jenkins): 28080
Nexus: 18081
